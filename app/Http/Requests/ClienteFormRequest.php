<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:100',
            'fecha_nacimiento' => 'required|date|before_or_equal:today',
            'correo' => 'required|email:rfc,dns'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Error, el campo nombre es necesario.'
        ];
    }
}
