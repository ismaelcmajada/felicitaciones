<?php

namespace App\Http\Controllers;

use App\ModelCliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelCliente  $modelCliente
     * @return \Illuminate\Http\Response
     */
    public function show(ModelCliente $modelCliente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelCliente  $modelCliente
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelCliente $modelCliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelCliente  $modelCliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelCliente $modelCliente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelCliente  $modelCliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelCliente $modelCliente)
    {
        //
    }
}
