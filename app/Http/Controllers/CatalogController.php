<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ClienteFormRequest;
use App\Models\Cliente;
use Illuminate\Support\Facades\Validator;

class CatalogController extends Controller
{

    public function getIndex() {
        return view('catalog/index', array('arrayClientes'=>Cliente::all()));
    }

    public function getShow($id) {
        $cliente = Cliente::findOrFail($id);
        return view('catalog/show', compact('id', 'cliente'));
    }

    public function getCreate() {
        return view('catalog/create');
    }

    public function getEdit($id) {
        $cliente = Cliente::findOrFail($id);
        return view('catalog/edit', compact('cliente'));
    }

    public function postCreate(Request $request) {

        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:100',
            'fecha_nacimiento' => 'required|date|before_or_equal:today',
            'correo' => 'required|email:rfc,dns'
        ]);

        if ($validator->fails()) {
            return redirect('catalog/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $cliente = new Cliente;
        $cliente->nombre = $request->nombre;

        if ($request->hasFile('imagen')) {
            if ($request->file('imagen')->isValid()) {
        
                $path = $request->imagen->store('images', 'public');
                $cliente->imagen = asset('storage/'.$path);

            }
        }

        $cliente->fecha_nacimiento = $request->fecha_nacimiento;
        $cliente->correo = $request->correo;
        $cliente->save();
        return redirect()->action('CatalogController@getIndex');
    }

    public function putEdit($id, ClienteFormRequest $request) {
        $cliente = Cliente::findOrFail($id);;
        $cliente->nombre = $request->nombre;

        if ($request->hasFile('imagen')) {
            if ($request->file('imagen')->isValid()) {
        
                $path = $request->imagen->store('images', 'public');
                $cliente->imagen = asset('storage/'.$path);
                
            }
        }

        $cliente->fecha_nacimiento = $request->fecha_nacimiento;
        $cliente->correo = $request->correo;
        $cliente->save();

        $validated = $request->validated();
        
        return redirect()->action('CatalogController@getShow', $id);
    }

    public function putDelete($id) {
        $cliente = Cliente::findOrFail($id);;
        $cliente->delete();
        return redirect()->action('CatalogController@getIndex');
    }
}
