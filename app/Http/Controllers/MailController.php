<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Mail;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MailController extends Controller
{
    public function basic_email() {
        $data = array('name'=>"Virat Gandhi");
     
        Mail::send(['text'=>'mail'], $data, function($message) {
           $message->to('ismaelcmajada@gmail.com', 'Tutorials Point')->subject
              ('Laravel Basic Testing Mail');
           $message->from('xyz@gmail.com','Virat Gandhi');
        });
        echo "Basic Email Sent. Check your inbox.";
     }
     public function html_email() {
        $data = array('name'=>"Virat Gandhi");
        Mail::send('mail', $data, function($message) {
           $message->to('ismaelcmajada@gmail.com', 'Tutorials Point')->subject
              ('Laravel HTML Testing Mail');
           $message->from('xyz@gmail.com','Virat Gandhi');
        });
        echo "HTML Email Sent. Check your inbox.";
     }
     public function attachment_email() {
        $data = array('name'=>"Virat Gandhi");
        Mail::send('mail', $data, function($message) {
           $message->to('ismaelcmajada@gmail.com', 'Tutorials Point')->subject
              ('Laravel Testing Mail with Attachment');
           $message->attach('C:\laravel-master\laravel\public\uploads\image.png');
           $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
           $message->from('xyz@gmail.com','Virat Gandhi');
        });
        echo "Email Sent with attachment. Check your inbox.";
     }

     public function felicitar()  {
      $arrayClientes = DB::table('clientes')->whereMonth('fecha_nacimiento', date("n"))->whereDay('fecha_nacimiento', date("j"))->get();
      foreach ($arrayClientes as $cliente)
      {
        $nombreClientes = array('name'=> $cliente->nombre);
        Mail::send('mail', $nombreClientes, function ($message) use($cliente)
        {
            $message->to($cliente->correo, $cliente->nombre)->subject(env('APP_NAME'));
            $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_USERNAME'));
        });
      }

     }
}
