<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Mail;

class EnviarCorreo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'envia:correo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrayClientes = DB::table('clientes')->whereMonth('fecha_nacimiento', date("n"))->whereDay('fecha_nacimiento', date("j"))->get();
        foreach ($arrayClientes as $cliente)
        {
            $nombreClientes = array('name'=> $cliente->nombre);
            Mail::send('mail', $nombreClientes, function ($message) use($cliente)
            {
                $message->to($cliente->correo, $cliente->nombre)->subject(env('APP_NAME'));
                $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_USERNAME'));
            });
        }

    }
}
