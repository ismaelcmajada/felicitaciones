@extends('layouts.master')

@section('content')

<div class="row">

    <div class="col-sm-4">

        <img src="{{$cliente->imagen}}" class="img-fluid" />

    </div>
    <div class="col-sm-8">

        <h1>{{$cliente->nombre}}</h1>
        <p><b>Correo-e:</b> {{$cliente->correo}}</p>
        <p><b>Fecha de nacimiento:</b> {{$cliente->fecha_nacimiento}}</p>

        <a href="{{ url('/catalog/edit/' . $cliente->id ) }}" class="btn btn-warning">Editar</a>
        <form action="{{action('CatalogController@putDelete', $cliente->id)}}" method="POST" style="display:inline">
            {{ method_field('DELETE') }}
            {!! csrf_field() !!}
            <button type="submit" class="btn btn-danger" style="display:inline">
                Borrar
            </button>
        </form>
        <a href="{{url()->previous()}}" class="btn btn-primary" >< Volver</a>

    </div>
</div>

@stop