@extends('layouts.master')

@section('content')

<div class="row" style="margin-top:40px">
    <div class="offset-md-3 col-md-6">
        <div class="card">
            <div class="card-header text-center">
                Añadir cliente
            </div>
            <div class="card-body" style="padding:30px">

                @if ($errors->any())

                <div class="row justify-content-center">

                    <div class="col-sm-7">

                        <div class="alert alert-danger">

                            <ul>

                                @foreach($errors->all() as $error)

                                <li>{{$error}}</li>

                                @endforeach

                            </ul>

                        </div>

                    </div>

                </div>

                @endif


                <form action="" method="post" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="title">Nombre</label>
                        <input type="text" name="nombre" id="nombre" value="{{old('nombre')}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="imagen">Imagen</label>
                        <input type="file" name="imagen" id="imagen" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="fecha_nacimiento">Fecha de nacimiento</label>
                        <input type="date" name="fecha_nacimiento" id="fecha_nacimiento" value="{{old('fecha_nacimiento')}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="Correo">Correo</label>
                        <input type="email" name="correo" id="correo" value="{{old('correo')}}" class="form-control">
                    </div>

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                            Añadir cliente
                        </button>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>

@stop
