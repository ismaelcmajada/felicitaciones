<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Cliente;
use Faker\Generator as Faker;

$factory->define(Cliente::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
        'imagen' => null,
        'fecha_nacimiento' => now(),
        'correo' => $faker->unique()->safeEmail
    ];
});
