<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/prueba404', function () {
    return view('prueba404');
});

Route::get('/request', 'PruebaRequest@index');

Route::get('/', 'HomeController@index')->middleware('language');

Route::get('auth/login', function () {
    return view('auth/login');
});

Route::get('auth/logout', function () {
    return 'Logout usuario';
});

Auth::routes(['verify' => 'true']);

Route::group(['middleware' => 'verified'], function () {

// Rutas a verificar

    Route::get('catalog', 'CatalogController@getIndex')->middleware('auth');

    Route::get('catalog/show/{id}', 'CatalogController@getShow')->middleware('auth');

    Route::get('catalog/create', 'CatalogController@getCreate')->middleware('auth');

    Route::post('catalog/create', 'CatalogController@postCreate')->middleware('auth');

    Route::get('catalog/edit/{id}', 'CatalogController@getEdit')->middleware('auth');

    Route::put('catalog/edit/{id}', 'CatalogController@putEdit')->middleware('auth');

    Route::delete('catalog/delete/{id}', 'CatalogController@putDelete')->middleware('auth');

    //Mail

    Route::get('sendbasicemail','MailController@basic_email');
    Route::get('sendhtmlemail','MailController@html_email');
    Route::get('sendattachmentemail','MailController@attachment_email');
    Route::get('felicitar', 'MailController@felicitar');

});



Route::resource ('/prueba', 'PruebaController');








Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
